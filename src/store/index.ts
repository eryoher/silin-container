import createSagaMiddleware from "redux-saga";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import RootReducer from "../reducers";
import rootSaga from "../sagas";
import configureAxios from "../lib/AxiosInterceptors";

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
	const store = createStore(RootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
	sagaMiddleware.run(rootSaga);
	configureAxios(store);

	return store;
};

export type RootStore = ReturnType<typeof RootReducer>;

export default configureStore;
