import { combineReducers } from "redux";
import { notesReducer } from "./NotesReducer";
import { dogsReducer } from "./DogsReducers";

const rootReducer = combineReducers({
	notes: notesReducer,
	dogState: dogsReducer,
});

export default rootReducer;
