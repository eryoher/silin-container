import { FETCH_DOGO, FETCH_DOGO_SUCCESS, DoggosAction } from "./../constants/ActionsTypes";

export interface DogState {
	dogImg: string;
}

const initialState = {
	dogImg: "",
};

export function dogsReducer(state: DogState = initialState, action: DoggosAction) {
	switch (action.type) {
		case FETCH_DOGO: {
			return { ...state, dogImg: null };
		}
		case FETCH_DOGO_SUCCESS:
			return { ...state, dogImg: action.payload };
		default:
			return state;
	}
}
