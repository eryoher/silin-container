import React from "react";
import styles from "./SidebarUnselected.module.scss";

const SidebarUnselected = ({ label = "Texto menu" }) => {
	return (
		<div style={{ width: "100%" }} className={styles.sidebar}>
			<p className={styles.label}>{label}</p>
		</div>
	);
};

export default SidebarUnselected;
