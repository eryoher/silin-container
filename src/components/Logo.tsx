import React from "react";
import styles from "./Logo.module.scss";

const Logo = () => {
	return (
		<div className={styles.logo}>
			<div className={styles.grupo787}>
				<img alt='' className={styles.elipse84} src='https://static.overlay-tech.com/assets/135ba828-3079-4532-9282-dc42a5979ca4.svg' />
				<img alt='' className={styles.elipse85} src='https://static.overlay-tech.com/assets/28c811eb-9658-48fb-aa98-0b37936d9134.svg' />
				<img alt='' src='https://static.overlay-tech.com/assets/f71a5a3f-90bb-4de4-a36f-2a750fee429f.svg' />
			</div>
			<div className={styles.silin}>
				<img alt='' className={styles.trazado82} src='https://static.overlay-tech.com/assets/3d51badf-3ea9-4700-8f0f-a640894112db.svg' />
				<img alt='' className={styles.trazado79} src='https://static.overlay-tech.com/assets/2cb2950e-b00b-43dc-b790-80da4c71d990.svg' />
				<img alt='' className={styles.trazado82} src='https://static.overlay-tech.com/assets/2f2d6db0-2e49-45d0-8306-0a4077396fbc.svg' />
				<img alt='' className={styles.trazado82} src='https://static.overlay-tech.com/assets/43a6f3cd-6b57-4e69-83e0-8d87b44e44ed.svg' />
				<img alt='' className={styles.trazado82} src='https://static.overlay-tech.com/assets/dda20a26-fbe5-47ab-9b24-1bf5133ea4b1.svg' />
			</div>
		</div>
	);
};

export default Logo;
