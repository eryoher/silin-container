import React from "react";
import styles from "./SidebarSelected.module.scss";

const SidebarSelected = ({ label = "Texto menu" }) => {
	return (
		<div className={styles.sidebar} style={{ width: "100%" }}>
			<p className={styles.label}>{label}</p>
		</div>
	);
};

export default SidebarSelected;
