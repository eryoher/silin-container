import React from "react";
import styles from "./NotificationIconNotified.module.scss";

const NotificationIconNotified = () => {
	return (
		<div className={styles.notificationIcon}>
			<div className={styles.relativeWrapperOne}>
				<div className={styles.subtract} />
				<img alt='' className={styles.ellipse112} src='https://static.overlay-tech.com/assets/36753dd9-146b-4d7d-bedc-090f1b05d91a.svg' />
			</div>
			<img alt='' src='https://static.overlay-tech.com/assets/c0af9a49-ef25-4e71-8e1c-e6b3149125a0.svg' />
		</div>
	);
};

export default NotificationIconNotified;
