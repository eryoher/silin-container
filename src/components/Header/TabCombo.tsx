import React from "react";
import SingleTabBlue from "./SingleTabBlue";
import styles from "./TabCombo.module.scss";

const TabCombo = () => {
	return (
		<div className={styles.tabCombo}>
			<SingleTabBlue />
			<SingleTabBlue />
		</div>
	);
};

export default TabCombo;
