import React from "react";
import Logo from "../Logo";
import TabCombo from "./TabCombo";
import LoggedUser from "./LoggedUser";
import styles from "./Header.module.scss";

const Header = () => {
	return (
		<div style={{ height: 120, width: "100%" }} className={styles.sectionHeaderTabs}>
			<Logo />
			<TabCombo />
			<LoggedUser />
		</div>
	);
};

export default Header;
