import React from "react";
import styles from "./ArrowDown.module.scss";

const ArrowDown = () => {
	return (
		<div className={styles.arrowIcon}>
			<img alt='' src='https://static.overlay-tech.com/assets/07d95520-acb8-4b49-b2a3-f0468aa62b71.svg' />
		</div>
	);
};

export default ArrowDown;
