import React from "react";
import ArrowDown from "./ArrowDown";
import NotificationIconNotified from "./NotificationIconNotified";
import styles from "./LoggedUser.module.scss";

const LoggedUser = ({ userName = "Usuario Jikko" }) => {
	return (
		<div className={styles.loggedUser}>
			<div className={styles.frame347}>
				<div className={styles.profilePic} />
				<p className={styles.userName}>{userName}</p>
				<ArrowDown />
			</div>
			<NotificationIconNotified />
		</div>
	);
};

export default LoggedUser;
