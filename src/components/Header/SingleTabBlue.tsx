import React from "react";
import styles from "./SingleTabBlue.module.scss";

const SingleTabBlue = ({ tabLabel = "Selected tab" }) => {
	return (
		<div className={styles.singleTab}>
			<p className={styles.tabLabel}>{tabLabel}</p>
			<div className={styles.tabSelector} />
		</div>
	);
};

export default SingleTabBlue;
