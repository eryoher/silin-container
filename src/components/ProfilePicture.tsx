import React from "react";
import styles from "./ProfilePicture.module.scss";

const ProfilePicture = () => {
	return (
		<div className={styles.profilePicture}>
			<img alt='' src='https://static.overlay-tech.com/assets/9ff033f5-3fb1-4e7b-99b5-cd87799ee2e1.png' />
		</div>
	);
};

export default ProfilePicture;
