import React from "react";
import styles from "./ArrowIcon.module.scss";

const ArrowIcon = () => {
	return (
		<div className={styles.arrowIcon}>
			<img alt='' src='https://static.overlay-tech.com/assets/06ffd96a-84a5-467c-a087-63de36dc8d82.svg' />
		</div>
	);
};

export default ArrowIcon;
