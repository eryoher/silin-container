import React from "react";
import ArrowIcon from "./ArrowIcon";
import ProfilePicture from "./ProfilePicture";
import styles from "./SidebarHeader.module.scss";

const SidebarHeader = ({ volver = "Volver", nombreAlcaldia = "Alcaldía de Palmira" }) => {
	return (
		<div className={styles.sidebarHeader}>
			<div className={styles.frame366}>
				<ArrowIcon />
				<p className={styles.volver}>{volver}</p>
			</div>
			<div className={styles.frame367}>
				<ProfilePicture />
				<p className={styles.name}>{nombreAlcaldia}</p>
			</div>
		</div>
	);
};

export default SidebarHeader;
