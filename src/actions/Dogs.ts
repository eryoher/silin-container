import { fetchDoggoAction, fetchDoggoSuccessAction, FETCH_DOGO, FETCH_DOGO_SUCCESS } from "../constants/ActionsTypes";

export const fetchDoggo = (): fetchDoggoAction => ({
	type: FETCH_DOGO,
});

export const fetchDoggoSuccess = (dog: string): fetchDoggoSuccessAction => ({
	type: FETCH_DOGO_SUCCESS,
	payload: dog,
});
