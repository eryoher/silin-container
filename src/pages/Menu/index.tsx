import React from "react";
import SidebarHeader from "../../components/SidebarHeader";
import Logo from "../../components/Logo";
import SidebarUnselected from "../../components/SidebarUnselected";
import SidebarSelected from "../../components/SidebarSelected";
import styles from "./Menu.module.scss";

const Sidebar = ({ modulos = "Módulos" }) => {
	return (
		<div className={styles.sidebar}>
			<SidebarHeader />
			<div className={styles.frame25}>
				<div className={styles.frame365}>
					<p className={styles.modulos}>{modulos}</p>
				</div>
				<SidebarUnselected />
				<SidebarSelected />
			</div>
			<div className={styles.frame349}>
				<Logo />
			</div>
		</div>
	);
};

export default Sidebar;
