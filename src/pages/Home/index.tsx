import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Menu from "../Menu";
import Header from "../../components/Header/Header";
import MicroFrontend from "../../lib/MicroFrontend";
import { useSelector } from "react-redux";
import { RootStore } from "../../store/";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: "flex",
		},
		appBar: {
			width: `calc(100% - ${drawerWidth}px)`,
			marginLeft: drawerWidth,
		},
		drawer: {
			width: drawerWidth,
			flexShrink: 0,
		},
		drawerPaper: {
			width: drawerWidth,
		},
		// necessary for content to be below app bar
		toolbar: theme.mixins.toolbar,
		content: {
			flexGrow: 1,
			backgroundColor: theme.palette.background.default,
			padding: theme.spacing(3),
		},
	})
);

const Login = () => {
	return <MicroFrontend name='Login' host={"http://localhost:3001"} />;
};

export default function Home() {
	const classes = useStyles();
	const { data } = useSelector((state: RootStore) => state.dogState);

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar position='fixed' color={"inherit"} className={classes.appBar}>
				<Toolbar>
					<Header />
				</Toolbar>
			</AppBar>
			<Drawer
				className={classes.drawer}
				variant='permanent'
				classes={{
					paper: classes.drawerPaper,
				}}
				anchor='left'
			>
				<Menu />
			</Drawer>
			<main className={classes.content}>
				<div className={classes.toolbar} />
				<div style={{ marginTop: "50px" }}>
					<Login />
				</div>
			</main>
		</div>
	);
}
