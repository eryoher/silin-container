export const fetchDoggo = async () => {
	//const response = await Axios.get('/Clientes/Consulta', { params });
	//return response.data;
	const url = "https://dog.ceo/api/breeds/image/random";

	const response = await fetch(url)
		.then((res) => res.json())
		.then((dogInfo) => {
			return dogInfo;
		});

	return response.message;
};
