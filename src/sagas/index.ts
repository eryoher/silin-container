import { all } from "redux-saga/effects";

import DogsSaga from "./Dogs";

export default function* rootSaga() {
	yield all([DogsSaga()]);
}
