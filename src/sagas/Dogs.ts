import { fetchDoggoSuccess } from "./../actions/Dogs";
import { fetchDoggo } from "./../apis/Dogs";
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { FETCH_DOGO } from "./../constants/ActionsTypes";

function* fetchDoggoRequest() {
	try {
		const dogData = yield call(fetchDoggo);
		yield put(fetchDoggoSuccess(dogData));
	} catch (error) {}
}

export function* fetchDoggoSaga() {
	yield takeEvery(FETCH_DOGO, fetchDoggoRequest);
}

export default function* rootSaga() {
	yield all([fork(fetchDoggoSaga)]);
}
