import React from "react";
import { Provider } from "react-redux";
import configureStore from "./store";
import Home from "./pages/Home";

function App() {
	const store = configureStore();

	return (
		<Provider store={store}>
			<Home />
		</Provider>
	);
}

export default App;
