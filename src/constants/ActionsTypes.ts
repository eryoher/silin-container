export const ADD_NOTE = "ADD_NOTE";
export const FETCH_DOGO = "FETCH_DOGO";
export const FETCH_DOGO_SUCCESS = "FETCH_DOGO_SUCCESS";

export interface addNoteAction {
	type: typeof ADD_NOTE;
	payload: string;
}

export interface fetchDoggoAction {
	type: typeof FETCH_DOGO;
}

export interface fetchDoggoSuccessAction {
	type: typeof FETCH_DOGO_SUCCESS;
	payload: string;
}

export interface DoggosAction {
	type: string;
	payload: string;
}
