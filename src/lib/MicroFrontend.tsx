import React, { useEffect } from "react";

export interface imicro {
	name: string;
	host: string;
}

function MicroFrontend(params: imicro) {
	const { name, host } = params;

	useEffect(() => {
		const scriptId = `micro-frontend-script-${name}`;

		const renderMicroFrontend = () => {
			window.renderLogin(`Login-container`);
			//window[`render${name}`](`${name}-container`);
		};

		if (document.getElementById(scriptId)) {
			renderMicroFrontend();
			return;
		}

		fetch(`${host}/asset-manifest.json`)
			.then((res) => res.json())
			.then((manifest) => {
				const script = document.createElement("script");
				script.id = scriptId;
				script.crossOrigin = "";
				script.src = `${host}${manifest.files["main.js"]}`;
				script.onload = () => {
					renderMicroFrontend();
				};
				document.head.appendChild(script);
			});

		return () => {
			//window[`unmount${name}`] && window[`unmount${name}`](`${name}-container`);
		};
	});

	return <main id={`Login-container`} />;
}

MicroFrontend.defaultProps = {
	document,
	window,
};

export default MicroFrontend;
