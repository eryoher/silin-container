# production environment
FROM nginx:1.19.4-alpine
COPY ./build /usr/share/nginx/html
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]